::mods_hookBaseClass("events/event", function(o)
{
	::modCHEV.F.registerAllFunctions(o);

	// Prevents errors when Events forget the start() function
	local oldSetScreen = o.setScreen;
	o.setScreen = function ( _screen )
	{
		if(("start" in _screen) == false) 	
		{
			_screen.start <- function(_start) {};
		}
		oldSetScreen(_screen);
	}
});

