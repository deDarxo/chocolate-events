::modCHEV.F.registerAllFunctions <- function(_hookedClass)
{
    ::modCHEV.F.registerAddFunctions(_hookedClass);
    ::modCHEV.F.registerVerboseFunctions(_hookedClass);
    ::modCHEV.F.registerRollFunctions(_hookedClass);
}

// Functions that manage adding and removing of most ressources during Events
::modCHEV.F.registerAddFunctions <- function(_hookedClass)
{
	_hookedClass.addTools <- function(_amount, _result)
	{
		if (_amount == 0) return;
		this.World.Assets.addArmorParts(_amount);
		_result.push({
			id = 10,
			icon = "ui/icons/asset_supplies.png",
			text = "You " + ::modCHEV.S.getGainLose(_amount) + " " + ::modCHEV.S.colorize(::modCHEV.S.getSign(_amount) + this.Math.abs(_amount), _amount) + " Tools"
		});
	}

	_hookedClass.addMedicine <- function(_amount, _result)
	{
		if (_amount == 0) return;
		this.World.Assets.addMedicine(_amount);
		_result.push({
			id = 10,
			icon = "ui/icons/asset_medicine.png",
			text = "You " + ::modCHEV.S.getGainLose(_amount) + " " + ::modCHEV.S.colorize(::modCHEV.S.getSign(_amount) + this.Math.abs(_amount), _amount) + " Medical Supplies"
		});
	}

	_hookedClass.addAmmo <- function(_amount, _result)
	{
		if (_amount == 0) return;
		this.World.Assets.addAmmo(_amount);
		_result.push({
			id = 10,
			icon = "ui/icons/asset_ammo.png",
			text = "You " + ::modCHEV.S.getGainLose(_amount) + " " + ::modCHEV.S.colorize(::modCHEV.S.getSign(_amount) + this.Math.abs(_amount), _amount) + " Ammunition"
		});
	}

	_hookedClass.addItem <- function(_item, _result)
	{
		if (typeof _item == "string") _item = this.new(_item);
		this.World.Assets.getStash().add(_item);		// Maybe include handling for when stash is full?
		_result.push({
			id = 10,
			icon = "ui/items/" + _item.getIcon(),
			text = "You gain " + _item.getName()
		});
	}

	_hookedClass.addMoney <- function(_amount, _result)
	{
		if (_amount == 0) return;
		this.World.Assets.addMoney(_amount);
		_result.push({
			id = 10,
			icon = "ui/icons/asset_money.png",
			text = "You " + ::modCHEV.S.getGainLose(_amount) + " " + ::modCHEV.S.colorize(::modCHEV.S.getSign(_amount) + this.Math.abs(_amount), _amount) + " Crowns"
		});
	}

	_hookedClass.addMorale <- function(_amount, _result, _reason = "")
	{
		if (_amount == 0) return;
		local oldMoralReputation = this.World.Assets.getMoralReputation();
		this.World.Assets.addMoralReputation(_amount);
		local newMoralReputation = this.World.Assets.getMoralReputation();
		local difference = newMoralReputation - oldMoralReputation;

		local iconPath = (difference >= 0) ? "ui/tooltips/positive.png" : "ui/tooltips/negative.png";
		_result.push({
			id = 8,
			icon = iconPath,
			text = "Your moral reputation " + ::modCHEV.S.getChangingWord(difference) + "s by " + ::modCHEV.S.colorize(::modCHEV.S.getSign(difference) + this.Math.abs(difference), difference)
		});

		_result.push({
			id = 9,
			icon = "ui/icons/asset_moral_reputation.png",
			text = "Your morale has changed from: " + oldMoralReputation + " to " + ::modCHEV.S.colorize(newMoralReputation, difference)
		});
	}

	_hookedClass.addRenown <- function(_amount, _result)
	{
		if (_amount == 0) return;
		this.World.Assets.addBusinessReputation(_amount);
		_result.push({
			id = 10,
			icon = "ui/icons/special.png",
			text = "Your renown " + ::modCHEV.S.getChangingWord(_amount) + "s by " + ::modCHEV.S.colorize(::modCHEV.S.getSign(_amount) + this.Math.abs(_amount), _amount)
		});
	}

	_hookedClass.addRelation <- function(_amount, _faction, _result, _reason = "")
	{
		if (_amount == 0) return;
		_faction.addPlayerRelation(_amount, _reason);

		_result.push({
			id = 10,
			icon = "ui/icons/relations.png",
			text = "Your relations to " + _faction.getName() + " " + ::modCHEV.S.getChangingWord(_amount) + "s by " + ::modCHEV.S.colorize(::modCHEV.S.getSign(_amount) + this.Math.abs(_amount), _amount)
		});
	}
}

// Functions for addtion verbose details after certain options during Events
::modCHEV.F.registerVerboseFunctions <- function(_hookedClass)
{
    _hookedClass.m.VerboseSuffixes <- true;
	_hookedClass.m.QuitMessage <- " (Quit)";
	_hookedClass.m.StartBattleMessage <- " (Start Battle)";
	_hookedClass.m.GainBrotherMessage <- " (Gain Battle Brother)";

    _hookedClass.setVerbose <- function(_verbose)
	{
		this.m.VerboseSuffixes = _verbose;
	}

	_hookedClass.getVerbose <- function()
	{
		return this.m.VerboseSuffixes;
	}

	_hookedClass.getQuitMessage <- function()
	{
		if(getVerbose() == false) return "";
		return this.m.QuitMessage;
	}

	_hookedClass.getStartBattleMessage <- function()
	{
		if(getVerbose() == false) return "";
		return this.m.StartBattleMessage;
	}

	_hookedClass.getGainBrotherMessage <- function()
	{
		if(getVerbose() == false) return "";
		return this.m.GainBrotherMessage;
	}
}

// Functions for rolling random rolls
::modCHEV.F.registerRollFunctions <- function(_hookedClass)
{
    // Random roll - Charisma: You/Your brothers trying to convince some entitiy (usually human) from behaving a certain way
	_hookedClass.rollCharisma <- function(_min = 1, _max = 100, _target = 1)
	{
		return ::Math.rand(_min, _max);
	}

    // Random roll - Skill: You/Your brothers trying to pass some skill check (that is not Charisma)
	_hookedClass.rollSkill <- function(_min = 1, _max_ = 100, _target = 1)
	{
		return ::Math.rand(_min, _max);
	}

    // Random roll - Luck: Choosing between different outcomes where one favors the player
	_hookedClass.rollLuck <- function(_min = 1, _max_ = 100, _target = 1)
	{
		return ::Math.rand(_min, _max);
	}

    // Random roll - Rand: Random roll that has no clear perference on the outcome
	_hookedClass.rollRand <- function(_min = 1, _max = 100)
	{
		return ::Math.rand(_min, _max);
	}
}

