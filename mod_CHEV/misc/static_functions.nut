::modCHEV.S <- {

    function clamp( _a, _min, _max )     // makes sure that _a remains between _min and _max
	{
        if(_min > _max) return clamp(a, _max, _min)
        return this.Math.min(this.Math.max(_a, _min), _max);
	}

    function posClamp( _a )     // makes sure that _a remains in the positive range
	{
        return this.Math.max(_a, 0);
	}

    function negClamp( _a )     // makes sure that _a remains in the negative range
	{
        return this.Math.min(_a, 0);
	}

    function automaticClamp( _oldA, _newA )     // makes sure that _newA stays with the same algebraic-sign-range as _oldA was
	{
        if(_oldA < 0) return negClamp( _newA );
        return posClamp( _newA );
	}

    function colorize(_valueString, _value)
    {
        local color = (_value >= 0) ? this.Const.UI.Color.PositiveValue : this.Const.UI.Color.NegativeValue;
        return "[color=" + color + "]" + _valueString + "[/color]";
    }

    function getSign (_value)
    {
        if(_value == 0) return "";
        return (_value > 0) ? "+" : "-";
    }

    function getChangingWord( _value, _capitalize = false )
    {
        local ret = "decrease";
        if(_value >= 0) ret = "increase";
        if(_capitalize) ret = ::MSU.String.capitalizeFirst(ret);
        return ret;
    }

    function getGainLose( _value, _capitalize = false )
    {
        local ret = "lose"
        if(_value >= 0) ret = "gain";
        if(_capitalize) ret = ::MSU.String.capitalizeFirst(ret);
        return ret;
    }
}

