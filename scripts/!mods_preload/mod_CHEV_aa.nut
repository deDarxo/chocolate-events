::modCHEV <- {
	ID = "mod_CHEV",
	Name = "Chocolate Events",
	Version = "0.1.0",
	F = {}
}

::mods_registerMod(::modCHEV.ID, ::modCHEV.Version, ::modCHEV.Name);

::mods_queue(::modCHEV.ID, "mod_msu", function()
{
	::modCHEV.Mod <- ::MSU.Class.Mod(::modCHEV.ID, ::modCHEV.Version, ::modCHEV.Name);

	::include("mod_CHEV/misc/static_functions");	// Utility Functions
	::include("mod_CHEV/misc/event_functions");		// Functions to automatically register some new functions into the event script

	::include("mod_CHEV/hooks/mod_event");
});

